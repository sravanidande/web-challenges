/* eslint-disable react/jsx-no-bind */
import React from 'react'
import './App.css'

type ImgIndex = 0 | 1 | 2 | 3

export const Panels: React.FC = () => {
  const [selected, setSelected] = React.useState<ImgIndex>(0)

  return (
    <div className="panels">
      <div
        className={
          selected === 1
            ? 'panel panel-first panel-focus'
            : 'panel panel-first panel-normal'
        }
        onClick={() => {
          if (selected === 1) {
            setSelected(0)
          } else {
            setSelected(1)
          }
        }}
      ></div>
      <div
        className={
          selected === 2
            ? 'panel panel-second panel-focus'
            : 'panel panel-second panel-normal'
        }
        onClick={() => {
          if (selected === 2) {
            setSelected(0)
          } else {
            setSelected(2)
          }
        }}
      ></div>
      <div
        className={
          selected === 3
            ? 'panel panel-third panel-focus'
            : 'panel panel-third panel-normal'
        }
        onClick={() => {
          if (selected === 3) {
            setSelected(0)
          } else {
            setSelected(3)
          }
        }}
      ></div>
    </div>
  )
}
