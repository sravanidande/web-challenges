/* eslint-disable react/jsx-no-bind */
import React from 'react'
import './App.css'

interface Timer {
  readonly timerText: string
  readonly time: string
  readonly buttonText: string
}

interface TimerProps {
  readonly timer: Timer
  onTimerClick(timer: Timer): void
}

export const TimerView: React.FC<TimerProps> = ({ timer, onTimerClick }) => (
  <div className="container">
    <h1 className="title">{timer.timerText}</h1>
    <div className="timer">{timer.time}</div>
    <button onClick={() => onTimerClick(timer)}>{timer.buttonText}</button>
  </div>
)
