import React from 'react'
import './App.css'

export function roll(min: number, max: number, floatFlag?: number) {
  let r = Math.random() * (max - min) + min
  return floatFlag ? r : Math.floor(r)
}

export let testPurses = Array(5)
  .fill(0)
  .map(_ => {
    return {
      quarters: roll(0, 15),
      dimes: roll(0, 30),
      nickels: roll(0, 40),
      pennies: roll(0, 50),
      price: Number(roll(0, 10, 1).toFixed(2)),
    }
  })

interface randomChange {
  readonly quarters: number
  readonly dimes: number
  readonly nickels: number
  readonly pennies: number
  readonly price?: number
}

interface ShortChangedProps {
  readonly change: randomChange
}

export const totalPriceCalculator = (change: randomChange): any => {
  return (
    change.quarters * 0.25 +
    change.nickels * 0.1 +
    change.dimes * 0.05 +
    change.pennies * 0.01
  ).toFixed(2)
}

export const ShortChanged: React.FC<ShortChangedProps> = () => {
  const [index, setIndex] = React.useState(0)
  const [purchaseStatus, setPurchaseStatus] = React.useState('')
  const [style, setStyle] = React.useState({})

  const change = testPurses[index]
  const totalPrice = totalPriceCalculator(change)

  React.useEffect(() => {
    if (totalPrice >= change.price) {
      setStyle({ backgroundColor: 'green' })
      setPurchaseStatus(
        `with ${totalPrice} in coins you can afford this ${change.price} purchase 😃`,
      )
    } else {
      setStyle({ backgroundColor: 'red' })
      setPurchaseStatus(
        `with ${totalPrice} in coins you cannot afford this ${change.price} purchase 😔`,
      )
    }
  }, [change])

  return (
    <>
      <div className="coins">
        <div className="coin quarter">.25</div>
        <div className="coin dime">.10</div>
        <div className="coin nickel">.05</div>
        <div className="coin penny">.01</div>
      </div>
      <div className="coin-counts">
        <div className="count">x{change.quarters}</div>
        <div className="count">x{change.nickels}</div>
        <div className="count">x{change.dimes}</div>
        <div className="count">x{change.pennies}</div>
      </div>
      <div className="input" style={style}>
        {purchaseStatus}
      </div>
      <div className="buttons">
        <button disabled={index <= 0} onClick={() => setIndex(index - 1)}>
          Previous
        </button>

        <button
          disabled={index >= testPurses.length - 1}
          onClick={() => setIndex(index + 1)}
        >
          Next
        </button>
      </div>
    </>
  )
}
