/* eslint-disable arrow-body-style */
import React from 'react'
import './App.css'

export const QuizMaker: React.FC = () => {
  return (
    <div className="container">
      <h1>It's Quiz Time!</h1>
      <p>Show off how smart you are with this awesome quiz.</p>
      <section>
        <p className="qtn">Which of these is not Spice Girl?</p>
        <ul>
          <li>Scary Spice</li>
          <li>Baby Spice</li>
          <li>Sporty Spice</li>
          <li>Pumpkin Spice</li>
        </ul>
      </section>
      <p>
        <a className="btn">Submit</a>
      </p>
    </div>
  )
}
