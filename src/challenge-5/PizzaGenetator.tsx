/* eslint-disable react/jsx-no-bind */
/* eslint-disable @typescript-eslint/promise-function-async */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable jsx-a11y/accessible-emoji */
/* eslint-disable arrow-body-style */
import React from 'react'
import './App.css'
// import { img } from '../challenge-4/img1.jpg'

export const getRandomArbitrary = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min) + min)
}

const pizzaId = getRandomArbitrary(0, 84)

export const PizzaGenerator: React.FC = () => {
  const [img, setImg] = React.useState('')
  const [pId, setPid] = React.useState(pizzaId)

  const handlePizzaClick = (id: number) => {
    setPid(id)
  }

  React.useEffect(() => {
    setImg(`https://foodish-api.herokuapp.com/images/pizza/pizza${pId}.jpg`)
  }, [pId])

  return (
    <div className="container">
      <h1>Random Pizza Generator 🍕 </h1>
      <button onClick={() => handlePizzaClick(getRandomArbitrary(0, 84))}>
        See Delicious Pizzas 😋
      </button>
      <img src={img} alt="Pizza on the way..." />
    </div>
  )
}
