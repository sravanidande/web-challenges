/* eslint-disable prefer-arrow/prefer-arrow-functions */

export function insertAt<T>(
  arr: readonly T[],
  idx: number,
  item: T,
): readonly T[] {
  return [...arr.slice(0, idx), item, ...arr.slice(idx)]
}

export function removeAt<T>(arr: readonly T[], idx: number): readonly T[] {
  return [...arr.slice(0, idx), ...arr.slice(idx + 1)]
}

export function replaceAt<T>(
  arr: readonly T[],
  idx: number,
  item: T,
): readonly T[] {
  return [...arr.slice(0, idx), item, ...arr.slice(idx + 1)]
}
