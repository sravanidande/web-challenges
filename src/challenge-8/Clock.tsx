/* eslint-disable arrow-body-style */
import { getHours, getMinutes, getSeconds } from 'date-fns'
import React from 'react'
import './App.css'

export const Clock: React.FC = () => {
  const [seconds, setSeconds] = React.useState(0)
  const [mins, setMins] = React.useState(0)
  const [hours, setHours] = React.useState(0)

  React.useEffect(() => {
    const id = setInterval(() => {
      setSeconds(getSeconds(new Date()))
      setMins(getMinutes(new Date()))
      setHours(getHours(new Date()))
    }, 1000)
    return () => clearInterval(id)
  }, [])

  const secondsDegree = (seconds / 60) * 360 + 90
  const minDegree = (mins / 60) * 360 + 90
  const hourDegree = hours * 30 + 90

  return (
    <>
      <h1>Hai, current time is:{`${hours} : ${mins}: ${seconds}`} </h1>
      <div className="clock">
        <div className="clock-face">
          <div
            className="hand hours"
            style={{ transform: `rotate(${hourDegree}deg)` }}
          ></div>
          <div
            className="hand minutes"
            style={{
              transform: `rotate(${minDegree}deg)`,
              backgroundColor: '#808080',
            }}
          ></div>
          <div
            className="hand seconds"
            style={{ transform: `rotate(${secondsDegree}deg)` }}
          ></div>
        </div>
      </div>
    </>
  )
}
