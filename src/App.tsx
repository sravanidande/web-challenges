import React from 'react'
import { TimerView } from './challenge-9/Timer'

// consst change = { quarters: 4, dimes: 25, nickels: 39, pennies: 24, price: 9.66 }

const timer = {
  timerText: 'Awesome Timer',
  time: '1:30',
  buttonText: 'startTimer',
}

export const App: React.FC = () => <TimerView timer={timer} />

// console.log(testPurses[0])
// console.log(totalPriceCalculator(testPurses[0]))
// console.log(getRandomArbitrary(0, 84))
