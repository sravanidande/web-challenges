/* eslint-disable arrow-body-style */
import React, { useEffect, useRef } from 'react'
import { tada } from 'react-animations'
import styled, { keyframes } from 'styled-components'
import './App.css'

const Bounce = styled.div`
  animation: 2s ${keyframes`${tada}`} infinite;
`

export const Rating: React.FC<{ handleClick(): void }> = ({ handleClick }) => {
  return (
    <div className="container" onClick={handleClick}>
      <p className="text">Click here to give your rating</p>
    </div>
  )
}

export const RatingOne: React.FC<{
  onKeyRating(evt: any): void
  readonly rating: string
}> = ({ onKeyRating, rating }) => {
  const style = rating === '' ? { display: 'block' } : { display: 'none' }

  const mainRef = useRef(null)
  useEffect(() => {
    mainRef.current.focus()
  }, [mainRef])

  console.log(rating, style)

  return (
    <div
      ref={mainRef}
      tabIndex={-1}
      className="container-one"
      onKeyPress={evt => onKeyRating(evt)}
      style={style}
    >
      <p className="text">Type a number between 1 to 5 </p>
    </div>
  )
}

export const EmojiRating: React.FC = () => {
  const [isNext, setIsNext] = React.useState(true)
  const [rating, setRating] = React.useState('')
  const [emojiText, setEmojiText] = React.useState('')

  const handleClick = () => {
    setIsNext(false)
  }

  const handleRating = (evt: any) => {
    switch (evt.key) {
      case '1':
        setRating('🤬')
        setEmojiText('VERY DISAPPOINTED')
        break
      case '2':
        setRating('☹️')
        setEmojiText('BAD')
        break
      case '3':
        setRating('😐')
        setEmojiText('GOOD, CAN DO BETTER')
        break
      case '4':
        setRating('🙂')
        setEmojiText('BETTER')
        break
      case '5':
        setRating('😁')
        setEmojiText('BEST')
        break
      default:
        setRating('')
    }
  }

  console.log(rating)

  return (
    <>
      <div>
        {isNext ? (
          <Rating handleClick={handleClick} />
        ) : (
          <RatingOne rating={rating} onKeyRating={handleRating} />
        )}
      </div>
      <Bounce>
        <h1 style={{ fontSize: '5rem', width: '100%', textAlign: 'center' }}>
          {rating}
        </h1>
      </Bounce>
      <p style={{ fontSize: '20px' }}>{emojiText}</p>
    </>
  )
}
