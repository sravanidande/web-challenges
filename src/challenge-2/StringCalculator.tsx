import React from 'react'
import './App.css'

export const SolutionView:React.FC<{readonly solution:number}> = ({solution}) => {
  return (
    <>
        <div className="solution">=</div>
        <div className="solution">{solution}</div> 
    </>

  )
}

export const StringCalculator:React.FC = () => {
  const [inputStr,setInputStr] = React.useState("")
  const [solution,setSolution] = React.useState<number|undefined>(undefined)

  const handleSolveClick = () => {
    setSolution(eval(inputStr))
  }

  console.log(solution)
  return (
    <div  className="container">
        <input type="text" className="string" value={inputStr} onChange={(evt)=>setInputStr(evt.target.value)}/>
        <button className="solve" onClick={handleSolveClick}>Solve</button>
        <div className="container-two">
        {inputStr.split("").map((it,index)=>(
          <div className="solution" key={index}>{it}</div>
        ))}
        {solution!==undefined ? <SolutionView solution={solution}/> : null}
    </div>
    </div>
  )
}

